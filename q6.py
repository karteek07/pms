import sys
from PyQt6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLineEdit, QPushButton, QLabel


class AdditionCalculator(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Simple Addition Calculator")

        # Create input fields
        self.input1 = QLineEdit()
        self.input2 = QLineEdit()

        # Create labels
        self.label1 = QLabel("Input 1:")
        self.label2 = QLabel("Input 2:")

        # Create button
        self.add_button = QPushButton("Add")

        # Create result label
        self.result_label = QLabel()

        # Connect button click event to handler function
        self.add_button.clicked.connect(self.add_numbers)

        # Set up layout
        layout = QVBoxLayout()
        layout.addWidget(self.label1)
        layout.addWidget(self.input1)
        layout.addWidget(self.label2)
        layout.addWidget(self.input2)
        layout.addWidget(self.add_button)
        layout.addWidget(self.result_label)
        
        self.setLayout(layout)

    def add_numbers(self):
        try:
            # Get input values
            num1 = float(self.input1.text())
            num2 = float(self.input2.text())

            # Perform addition
            result = num1 + num2

            # Update result label
            self.result_label.setText(f"Result: {result}")
        except ValueError:
            # Handle non-numeric input
            self.result_label.setText("Please enter valid numbers")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = AdditionCalculator()
    window.show()
    sys.exit(app.exec())
