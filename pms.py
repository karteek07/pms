import sys
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import *


# Create the Qt Application
app = QApplication(sys.argv)

# Layout
main_layout = QGridLayout()

# Labels
cb_label = QLabel('Select Operation: ')

# Combo box
cb = QComboBox()
cb.addItems(['Add', 'View', 'Edit', 'Delete'])
label = QLabel()



def handle_combobox_change(index):
    if index == 0:
        cb_label.setText("This is Layout 1")
    elif index == 1:
        cb_label.setText("This is Layout 2")
    elif index == 2:
        cb_label.setText("This is Layout 3")
    elif index == 3:
        cb_label.setText("This is Layout 4")

cb.currentIndexChanged.connect(handle_combobox_change)

# Adds
main_layout.addWidget(cb, 0, 1)#, alignment=Qt.AlignmentFlag.AlignTop | Qt.AlignmentFlag.AlignCenter)
main_layout.addWidget(cb_label, 0, 0)


# Window
window = QMainWindow()
container = QWidget()
container.setLayout(main_layout)
window.setCentralWidget(container)
window.setWindowTitle("Project Management System")
window.setGeometry(300, 150, 900, 600)
window.show()

# Run the event loop
sys.exit(app.exec())