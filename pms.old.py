# import sys
# from PyQt5.QtWidgets import QApplication, QMainWindow
import sys 
from PyQt5.QtCore import * 
from PyQt5.QtGui import * 
from PyQt5.QtWidgets import *


# Create the Qt Application
app = QApplication(sys.argv)


# Tab pages
def add_tab():
    page = QWidget()
    layout = QGridLayout(page)
    
    label = QLabel("Content for Add")
    layout.addWidget(label, 0, 0)
    
    button = QPushButton("Button on Add")
    layout.addWidget(button, 1, 0)
    
    return page


def view_tab():
    page = QWidget()
    layout = QGridLayout(page)
    
    label = QLabel("Content for View")
    layout.addWidget(label, 0, 0)
    
    button = QPushButton("Button on View")
    layout.addWidget(button, 1, 0)
    
    return page




# Tabs
tabs = QTabWidget()
tabs.addTab(add_tab(), 'Add')
tabs.addTab(view_tab(), 'View')


# Window
window = QMainWindow()
window.setCentralWidget(tabs)
window.setWindowTitle("Project Management System")
window.setGeometry(300,300, 800, 800)
window.show()


# Run the event loop
sys.exit(app.exec_())
