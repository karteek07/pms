import sys
from PyQt6.QtWidgets import *

class PMS(QWidget):
    def __init__(self):
        super().__init__()
        self.main_window()
        
    def main_window(self):
        self.setWindowTitle("Project Management System")
        layout = QGridLayout()
        selection = QComboBox()
        selection.addItems(['Add', 'View', 'Update', 'Delete'])
        self.setLayout(layout)


    def add_numbers(self):
        try:
            # Get input values
            num1 = float(self.input1.text())
            num2 = float(self.input2.text())

            # Perform addition
            result = num1 + num2

            # Update result label
            self.result_label.setText(f"Result: {result}")
        except ValueError:
            # Handle non-numeric input
            self.result_label.setText("Please enter valid numbers")




if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = PMS()
    window.setGeometry(200, 600, 900, 600)
    window.show()
    sys.exit(app.exec())