import sys
from PyQt5.QtWidgets import QApplication, QMainWindow

# Create the Qt Application
app = QApplication(sys.argv)

# Create the main window
window = QMainWindow()
window.setWindowTitle("Hello, PyQt5!")
window.show()

# Run the event loop
sys.exit(app.exec_())